﻿class ChitsController < ApplicationController
 def start
 
 end
  def index
    @chits=Chit.all
  end
  
  def new
    @chit=Chit.new
  end
  
  def create
    @chit=Chit.new(chit_params)
	if @chit.save
	 flash[:notice]="phiếu chi đã tạo"
	 redirect_to @chit
	else
	#nothing,yet
	end
  end
  
  def show
    @chit=Chit.find(params[:id])
  end
  
  def edit
    @chit = Chit.find(params[:id])
  end
  
  def update
   @chit=Chit.find(params[:id])
   @chit.update(chit_params)
   flash[:notice]="phiếu chi đã cập nhật xong"
   redirect_to @chit
  end
  
  def destroy
    @chit=Chit.find(params[:id])
	@chit.destroy
	flash[:notice]="phiếu chi đã được xóa"
	redirect_to chits_path
  end
  private
  def chit_params
    params.require(:chit).permit(:serial, :name, :address, :reason, :payment, :day, :month, :year, :ability, :dean, :tabolator, :payer, :cashier)
  end
end
