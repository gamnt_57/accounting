﻿#encoding UTF-8
module ApplicationHelper
 def title(*parts)
  unless parts.empty?
   content_for :title do
   (parts<< "PHIẾU CHI").join("-")
   end
  end
 end
end
