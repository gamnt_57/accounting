﻿#encoding UTF-8
require 'spec_helper'
feature "tạo phiếu thu" do
scenario "tạo 1 phiếu thu" do
 visit '/'
  click_link 'phiếu thu mới'
  fill_in 'số phiếu',with:'1'
  fill_in 'tên người nộp tiền',with:'hovaten'
  fill_in 'địa chỉ',with:'noitaophieu'
  fill_in 'lý do nộp',with:'nophocphi'
  fill_in 'số tiền',with:'200000'
  fill_in 'nguồn kinh phí',with:'doan'
  fill_in 'chủ nhiệm khoa',with:'hoten'
  fill_in 'người lập phiếu',with:'hoten'
  fill_in 'người nộp tiền',with:'hoten'
  fill_in 'thủ quỹ',with:'hoten'
  click_button'tạo phiếu'
  expect(page).to have_content('phiếu thu đã tạo')
 end
end