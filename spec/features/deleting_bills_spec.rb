﻿#encoding UTF-8

require 'spec_helper'

 feature "xóa phiếu thu" do
  scenario "xóa 1 phiếu thu" do
   FactoryGirl.create(:bill, name: "hovaten")
   
    visit bills_path
	click_link "hovaten"
	click_link "xóa phiếu thu"
	
	expect(page).to have_content("phiếu thu đã được xóa")
	
	visit bills_path
	
	expect(page).to have_no_content("hovaten")
   end
 end