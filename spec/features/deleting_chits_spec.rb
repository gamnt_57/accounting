﻿#encoding UTF-8

require 'spec_helper'

 feature "xóa phiếu chi" do
  scenario "xóa 1 phiếu chi" do
   FactoryGirl.create(:chit, name: "hovaten")
   
    visit chits_path
	click_link "hovaten"
	click_link "xóa phiếu chi"
	
	expect(page).to have_content("phiếu chi đã được xóa")
	
	visit"/"
	
	expect(page).to have_no_content("hovaten")
   end
 end
 