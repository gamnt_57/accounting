﻿#encoding UTF-8

require 'spec_helper'

feature "Sửa phiếu chi" do
 scenario "Cập nhật phiếu chi" do
   FactoryGirl.create(:chit, name: "hovaten")
   
   visit chits_path
   click_link "hovaten"
   click_link "sửa phiếu chi"
  fill_in 'số phiếu',with:'1'
  fill_in 'tên người tạo phiếu',with:'hovaten'
  fill_in 'địa chỉ',with:'noitaophieu'
  fill_in 'lý do nộp',with:'nophocphi'
  fill_in 'số tiền',with:'200000'
  fill_in 'ngày',with:'3'
  fill_in 'tháng',with:'3'
  fill_in 'năm',with:'1993'
  fill_in 'nguồn kinh phí',with:'doan'
  fill_in 'chủ nhiệm khoa',with:'hoten'
  fill_in 'người lập phiếu',with:'hoten'
  fill_in 'người nộp tiền',with:'hoten'
  fill_in 'thủ quỹ',with:'hoten'
  click_button "cập nhật phiếu chi"
   
   expect(page).to have_content("phiếu chi đã cập nhật xong")
  end
end