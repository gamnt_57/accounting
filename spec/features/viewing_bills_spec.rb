﻿#encoding UTF -8

require 'spec_helper'

feature "xem phiếu thu" do
 scenario "danh sách phiếu thu" do
  bill=FactoryGirl.create(:bill, name: "hovaten")
  visit bills_path
  click_link 'hovaten'
  expect(page.current_url).to eql(bill_url(bill))
 end
end