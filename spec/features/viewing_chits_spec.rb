﻿#encoding UTF -8

require 'spec_helper'

feature "xem phiếu chi" do
 scenario "danh sách phiếu chi" do
  chit=FactoryGirl.create(:chit, name: "hovaten")
  visit chits_path
  click_link 'hovaten'
  expect(page.current_url).to eql(chit_url(chit))
 end
end